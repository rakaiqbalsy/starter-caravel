<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Authentication
Route::get('/', [App\Http\Controllers\AuthenticationController::class, 'index'])->name('auth');
Route::post('/loginaction', [App\Http\Controllers\AuthenticationController::class, 'loginAct'])->name('loginaction');
Route::get('/logoutaction', [App\Http\Controllers\AuthenticationController::class, 'logoutAct'])->name('logoutaction')->middleware('auth');

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard')->middleware('auth');

// Korban
Route::get('/dashboard/korban', [App\Http\Controllers\KorbanController::class, 'index'])->name('dashboard.korban')->middleware('auth');
Route::get('/dashboard/korban/create', [App\Http\Controllers\KorbanController::class, 'create'])->name('dashboard.korban.create')->middleware('auth');

// Export Excel
Route::get('/dashboard/korban/export_excel', [App\Http\Controllers\KorbanController::class, 'export_excel'])->name('dashboard.korban.export_excel')->middleware('auth');

// Export PDF
Route::get('/dashboard/korban/cetak_pdf', [App\Http\Controllers\KorbanController::class, 'cetak_pdf'])->name('dashboard.korban.cetak_pdf')->middleware('auth');

Route::post('/dashboard/korban/store', [App\Http\Controllers\KorbanController::class, 'store'])->name('dashboard.korban.store')->middleware('auth');
Route::get('/dashboard/korban/destroy/{id}', [App\Http\Controllers\KorbanController::class, 'destroy'])->name('dashboard.korban.destroy')->middleware('auth');
Route::get('/dashboard/korban/delete/{id}', [App\Http\Controllers\KorbanController::class, 'delete'])->name('dashboard.korban.delete')->middleware('auth');

Route::get('/dashboard/korban/edit/{id}', [App\Http\Controllers\KorbanController::class, 'edit'])->name('dashboard.korban.edit')->middleware('auth');
Route::post('/dashboard/korban/update/{id}', [App\Http\Controllers\KorbanController::class, 'update'])->name('dashboard.korban.update')->middleware('auth');