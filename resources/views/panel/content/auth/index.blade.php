<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="{{ URL::asset('assets/amdash/assets/images/favicon-32x32.png') }}" type="image/png" />
    <!--plugins-->
    <link href="{{ URL::asset('assets/amdash/assets/plugins/notifications/css/lobibox.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" />
    <!-- loader-->
    <link href="{{ URL::asset('assets/amdash/assets/css/pace.min.css') }}" rel="stylesheet" />
    <script src="{{ URL::asset('assets/amdash/assets/js/pace.min.js') }}"></script>
    <!-- Bootstrap CSS -->
    <link href="{{ URL::asset('assets/amdash/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="{{ URL::asset('assets/amdash/assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/amdash/assets/css/icons.css') }}" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/amdash/assets/css/dark-theme.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/amdash/assets/css/semi-dark.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/amdash/assets/css/header-colors.css') }}" />
    <title>BPBD</title>
</head>

<body class="bg-login">
    <!--wrapper-->
    <div class="wrapper">
        <div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-0">
            <div class="container-fluid">
                <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                    <div class="col mx-auto">
                        <div class="mb-4 text-center">
                            <img src="{{URL::asset('assets/amdash/assets/images/logo-img.png')}}" width="180" alt="" />
                        </div>
                        <div class="card">
                            <div class="card-body">
                                    <div class="text-center">
                                        @if (session('error'))
                                        <div class="alert alert-danger">
                                            <b>Maaf, </b> {{session('error')}}
                                        </div>
                                        @endif
                                        <h3 class="">Masuk</h3>
                                        <p>Masuk Menggunakan Email Anda</p>
                                    </div>
                                    <div class="form-body">
                                        <form class="row g-3" action="/loginaction" method="POST">
                                        @csrf
                                            <div class="col-12">
                                                <label for="inputEmailAddress" class="form-label">Email</label>
                                                <input type="email" class="form-control" id="inputEmailAddress" name="email" placeholder="Email">
                                            </div>
                                            <div class="col-12">
                                                <label for="inputChoosePassword" class="form-label">Enter Password</label>
                                                <div class="input-group" id="show_hide_password">
                                                    <input type="password" class="form-control border-end-0" id="inputChoosePassword" name="password" placeholder="Masukan Password Anda"> <a href="javascript:;" class="input-group-text bg-transparent"><i class='bx bx-hide'></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                                    <label class="form-check-label" for="flexSwitchCheckChecked">Remember Me</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-end"> <a href="authentication-forgot-password.html">Forgot Password ?</a>
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>Sign in</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
    <!--end wrapper-->
    <!-- Bootstrap JS -->
    <script src="{{ URL::asset('assets/amdash/assets/js/bootstrap.bundle.min.js') }}"></script>
    <!--plugins-->
    <script src="{{ URL::asset('assets/amdash/assets/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/simplebar/js/simplebar.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>

    <script src="{{ URL::asset('assets/amdash/assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>

    <script src="{{ URL::asset('assets/amdash/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/chartjs/js/Chart.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/chartjs/js/Chart.extension.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
    <!--notification js -->
    <script src="{{ URL::asset('assets/amdash/assets/plugins/notifications/js/lobibox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/notifications/js/notifications.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/js/index.js') }}"></script>
    <!--Password show & hide js -->
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("bx-hide");
                    $('#show_hide_password i').removeClass("bx-show");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("bx-hide");
                    $('#show_hide_password i').addClass("bx-show");
                }
            });
        });
    </script>
    <!--app JS-->
    <script src="{{ URL::asset('assets/amdash/assets/js/app.js') }}"></script>
</body>