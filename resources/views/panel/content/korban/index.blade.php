@extends('panel.include.layout')
@section('content')

        <!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">KORPS</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Master Data KORPS</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="/dashboard/korban/create" class="btn btn-primary"><i class="bx bx-message-square-add"></i>Tambah Data</a>
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<h6 class="mb-0 text-uppercase">Data Korps</h6>
                <div class="ms-auto">
					<div class="btn-group">
						<a href="/dashboard/korban/cetak_pdf" class="btn btn-primary"><i class="bx bx-message-square-add"></i>Export Laporan</a>
					</div>
				</div>
				<hr/>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered">
                            <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No Identitas</th>
                                        <th>Nama Korban</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Kelurahan</th>
                                        <th>Kecamatan</th>
                                        <th>Kabupaten</th>
                                        <th>Tanggal Lahir</th>
                                        <th class="align-center text-center">Action</th>
                                </thead>
                                <tbody>
                                    {{-- Foreach start --}}
                                    @php $i=1 @endphp
                                    @foreach ($korban as $key => $s)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $s->no_identitas }}</td>
                                            <td>{{ $s->nama_korban }}</td>
                                            <td>{{ $s->jenis_kelamin }}</td>
                                            <td>{{ $s->alamat }}</td>
                                            <td>{{ $s->nama_kelurahan }}</td>
                                            <td>{{ $s->kecamatan }}</td>
                                            <td>{{ $s->kabupaten }}</td>
                                            <td>{{ $s->tanggal_lahir }}</td>
                                            <td class="align-center text-center">
                                                <a href="/dashboard/korban/edit/{{ $s->id }}">
                                                    <button type="button" class="btn btn-default">
                                                        Edit
                                                    </button>
                                                </a>
                                                <button data-item="{{ $s->id }}" type="button"
                                                    class="btn btn-danger" data-bs-toggle="modal"
                                                    data-bs-target="#delete-file-{{ $s->id }}">
                                                    Delete
                                                </button>
                                            </td> 
                                        </tr>
                                        <!-- Modal Delete -->
                                        <div class="modal fade" id="delete-file-{{ $s->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
                                                        <button type="button" data-bs-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <label>Apakah data ini akan dihapus {{ $s->nama_korban }}? data di DB
                                                            akan terhapus juga!!</label>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn bg-gradient-secondary"
                                                            data-bs-dismiss="modal">Close</button>
                                                        <a href="/dashboard/korban/destroy/{{ $s->id }}">
                                                            <button type="button"
                                                                class="btn btn-small btn-danger">Delete</button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Modal Delete -->

                                        {{-- Endforeach --}}
                                    @endforeach
                                </tbody>
								<tfoot>
                                <tr>
                                        <th>No</th>
                                        <th>No Identitas</th>
                                        <th>Nama Korban</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <td>Kelurahan</td>
                                        <td>Kecamatan</td>
                                        <td>Kabupaten</td>
                                        <td>Tanggal Lahir</td>
                                        <th class="align-center text-center">Action</th>
                                    </tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--end page wrapper -->
@stop
