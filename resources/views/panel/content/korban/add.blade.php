@extends('panel.include.layout')
@section('content')

        <!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">KORBAN</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Input Data Korban</li>
							</ol>
						</nav>
					</div>
				</div>
				<!--end breadcrumb-->
				<h6 class="mb-0 text-uppercase">Input Data Korban</h6>
				<hr/>
				<div class="card">
					<div class="card-body">
                    <form method="POST" action="/dashboard/korban/store" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                            <div class=col-4>
                                <div class="form-group-inner">
                                    <label>Nomer Identitas</label>
                                    <input type="number" class="form-control" name="no_identitas" id="no_identitas"
                                        placeholder="Input Kode Satker" required />
                                </div>
                            </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="form-group-inner">
                                        <label>Nama Korban</label>
                                        <input type="text" class="form-control" name="nama_korban" id="nama_korban"
                                            placeholder="Input Nama Korban" required />
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="form-group-inner">
                                        <label>Umur</label>
                                        <input type="number" class="form-control" name="umur" id="umur"
                                            placeholder="Input Umur Korban" required />
                                    </div>
                                </div>
                            </div>

                            <br>
                            
                            <div class="row">
                            <div class=col-4>
                                <div class="form-group-inner">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                        <option value="">Pilih Jenis Kelamin</option>
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                    
                                </div>
                            </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="form-group-inner">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control" name="alamat" id="alamat"
                                            placeholder="Input Alamat Korban" required />
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="form-group-inner">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir"
                                            placeholder="Input Tanggal Lahir" required />
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                            <div class=col-4>
                                <div class="form-group-inner">
                                    <label>Kabupaten</label>
                                    <select class="form-control" name="kabupaten" id="kabupaten">
                                        <option value="">Pilih Kabupaten</option>
                                        @foreach ($kabupaten as $kab)
                                        <option value="{{ $kab->id }}">{{ $kab->kabupaten }}</option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>

                            <div class=col-4>
                                <div class="form-group-inner">
                                    <label>Kecamatan</label>
                                    <select class="form-control" name="kecamatan" id="kecamatan">
                                        <option value="">Pilih Kecamatan</option>
                                        @foreach ($kecamatan as $kec)
                                        <option value="{{ $kec->id }}">{{ $kec->kecamatan }}</option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>

                            <div class=col-4>
                                <div class="form-group-inner">
                                    <label>Kelurahan</label>
                                    <select class="form-control" name="kelurahan" id="kelurahan">
                                        <option value="">Pilih Kelurahan</option>
                                        @foreach ($kelurahan as $kel)
                                        <option value="{{ $kel->id }}">{{ $kel->nama_kelurahan }}</option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>


                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-10">
                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Simpan</button>
                                </div>
                                <div class="col-lg-1">
                                    <a href="/dashboard/satker" class="btn btn-white">Batal</a>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
		<!--end page wrapper -->
@stop
