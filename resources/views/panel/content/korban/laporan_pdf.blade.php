<!DOCTYPE html>
<html>
<head>
	<title>Laporan Korban</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Korban</h4>
	</center>
    
 
	<table class='table table-bordered'>
    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kabupaten</th>
                                        <th>Balita</th>
                                        <th>Anak</th>
                                        <th>Remaja</th>
                                        <th>Dewasa</th>
                                        <th>Orang Tua</th>
                                </thead>
                                <tbody>
                                    {{-- Foreach start --}}
                                    @php $i=1 @endphp
                                    @foreach ($korban as $key => $s)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $s->kabupaten }}</td>
                                            <td>{{ $s->balita }}</td>
                                            <td>{{ $s->anak }}</td>
                                            <td>{{ $s->remaja }}</td>
                                            <td>{{ $s->dewasa }}</td>
                                            <td>{{ $s->ortu }}</td>
                                        {{-- Endforeach --}}
                                    @endforeach
                                </tbody>
	</table>
 
</body>
</html>