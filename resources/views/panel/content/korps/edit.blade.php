@extends('panel.include.layout')
@section('content')


    <div class="conatiner-fluid content-inner mt-n5 py-0">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Input Personel</h4>
                        </div>
                    </div>

                    <br>
                    <br>

                    <div class="card-body">
                        <form method="POST" action="/dashboard/satker/update/{{$satker->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class=col-12>
                                <div class="form-group-inner">
                                    <label>Kode Satker</label>
                                    <input type="text" class="form-control" name="kd_satker" id="kd_satker"
                                        placeholder="Input Kode Satker" value="{{$satker->kd_satker}}" required />
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group-inner">
                                        <label>Nama SATKER</label>
                                        <input type="text" class="form-control" name="nama_satker" id="nama_satker"
                                            placeholder="Input Nama Satker" value="{{$satker->nama_satker}}" required />
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-lg-10">
                                </div>
                                <div class="col-lg-2">
                                    <a href="/dashboard/satker" class="btn btn-white">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@stop
