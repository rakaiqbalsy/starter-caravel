@extends('panel.include.layout')
@section('content')


    <div class="conatiner-fluid content-inner mt-n5 py-0">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Input Personel</h4>
                        </div>
                    </div>

                    <br>
                    <br>

                    <div class="card-body">
                        <form method="POST" action="/dashboard/satker/update/{{$pangkat->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class=col-12>
                                <div class="form-group-inner">
                                    <label>Kode Satker</label>
                                    <input type="text" class="form-control" name="kd_pangkat" id="kd_pangkat"
                                        placeholder="Input Kode Satker" value="{{$pangkat->kd_pangkat}}" required />
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group-inner">
                                        <label>Nama SATKER</label>
                                        <input type="text" class="form-control" name="nama_pangkat" id="nama_pangkat"
                                            placeholder="Input Nama Satker" value="{{$pangkat->nama_pangkat}}" required />
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-lg-10">
                                </div>
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-1">
                                    <a href="/dashboard/satker" class="btn btn-white">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@stop
