<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="{{ URL::asset('assets/amdash/assets/images/favicon-32x32.png') }}" type="image/png" />
    <!--plugins-->
    <link href="{{ URL::asset('assets/amdash/assets/plugins/notifications/css/lobibox.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/amdash/assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" />
    <!-- loader-->
    <link href="{{ URL::asset('assets/amdash/assets/css/pace.min.css') }}" rel="stylesheet" />
    <script src="{{ URL::asset('assets/amdash/assets/js/pace.min.js') }}"></script>
    <!-- Bootstrap CSS -->
    <link href="{{ URL::asset('assets/amdash/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="{{ URL::asset('assets/amdash/assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/amdash/assets/css/icons.css') }}" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/amdash/assets/css/dark-theme.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/amdash/assets/css/semi-dark.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/amdash/assets/css/header-colors.css') }}" />

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <title>BPBD DKI | Dashboard</title>
</head>

<body onload="info_noti()">
    <!--wrapper-->
    <div class="wrapper">
        {{-- Sidebar --}}
        @include('panel.include.sidebar')
        {{-- End Side Bar --}}

        {{-- Header --}}
        @include('panel.include.header')
        {{-- EndHeader --}}

        {{-- Main --}}
        @yield('content')
        {{-- End Main --}}

        <!--start overlay-->
        <div class="overlay toggle-icon"></div>
        <!--end overlay-->

        <!--Start Back To Top Button-->
        <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->

        <!-- Footer Section Start -->
        @include('panel.include.footer')
        <!-- Footer Section End -->

    </div>
    {{-- End Wrapper --}}
    <!--start switcher-->
    <div class="switcher-wrapper">
        <div class="switcher-btn"> <i class='bx bx-cog bx-spin'></i>
        </div>
        <div class="switcher-body">
            <div class="d-flex align-items-center">
                <h5 class="mb-0 text-uppercase">Theme Customizer</h5>
                <button type="button" class="btn-close ms-auto close-switcher" aria-label="Close"></button>
            </div>
            <hr />
            <h6 class="mb-0">Theme Styles</h6>
            <hr />
            <div class="d-flex align-items-center justify-content-between">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="lightmode" checked>
                    <label class="form-check-label" for="lightmode">Light</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="darkmode">
                    <label class="form-check-label" for="darkmode">Dark</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="semidark">
                    <label class="form-check-label" for="semidark">Semi Dark</label>
                </div>
            </div>
            <hr />
            <div class="form-check">
                <input class="form-check-input" type="radio" id="minimaltheme" name="flexRadioDefault">
                <label class="form-check-label" for="minimaltheme">Minimal Theme</label>
            </div>
            <hr />
            <h6 class="mb-0">Header Colors</h6>
            <hr />
            <div class="header-colors-indigators">
                <div class="row row-cols-auto g-3">
                    <div class="col">
                        <div class="indigator headercolor1" id="headercolor1"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor2" id="headercolor2"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor3" id="headercolor3"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor4" id="headercolor4"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor5" id="headercolor5"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor6" id="headercolor6"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor7" id="headercolor7"></div>
                    </div>
                    <div class="col">
                        <div class="indigator headercolor8" id="headercolor8"></div>
                    </div>
                </div>
            </div>
            <hr />
            <h6 class="mb-0">Sidebar Colors</h6>
            <hr />
            <div class="header-colors-indigators">
                <div class="row row-cols-auto g-3">
                    <div class="col">
                        <div class="indigator sidebarcolor1" id="sidebarcolor1"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor2" id="sidebarcolor2"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor3" id="sidebarcolor3"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor4" id="sidebarcolor4"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor5" id="sidebarcolor5"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor6" id="sidebarcolor6"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor7" id="sidebarcolor7"></div>
                    </div>
                    <div class="col">
                        <div class="indigator sidebarcolor8" id="sidebarcolor8"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end switcher-->

    {{-- JS --}}
    <!-- Bootstrap JS -->
    <script src="{{ URL::asset('assets/amdash/assets/js/bootstrap.bundle.min.js') }}"></script>
    <!--plugins-->
    <script src="{{ URL::asset('assets/amdash/assets/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/simplebar/js/simplebar.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>

    <script src="{{ URL::asset('assets/amdash/assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>

    <script src="{{ URL::asset('assets/amdash/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/chartjs/js/Chart.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/chartjs/js/Chart.extension.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
    <!--notification js -->
    <script src="{{ URL::asset('assets/amdash/assets/plugins/notifications/js/lobibox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/amdash/assets/plugins/notifications/js/notifications.min.js') }}"></script>

    <script src="{{ URL::asset('assets/amdash/assets/js/index.js') }}"></script>

    <script>
            $(document).ready(function() {
             $('.jenis_kelamin').select2();
            });

            $(document).ready(function() {
             $('.kabupaten').select2();
            });

            $(document).ready(function() {
             $('.kelurahan').select2();
            });

            $(document).ready(function() {
             $('.kecamatan').select2();
            });
        </script>

    <script>
        $(document).ready(function() {
            var table = $('#example2').DataTable({
                lengthChange: false,
                buttons: ['copy', 'excel', 'pdf', 'print']
            });

            table.buttons().container()
                .appendTo('#example2_wrapper .col-md-6:eq(0)');
        });
    </script>

<script>
    $(function() {
        $('#dtserver').DataTable({
            processing: true,
            serverSide: true,
            // retrieve: true,
            ajax: '/dsp/data',
            // ajax: {
            //     url: '/dsp/data',
            //     dataSrc: 'data'
            // }, // memanggil route yang menampilkan data json
            columns: [{
                    data: 'id',
                    name: 'id',
                    // render: function(data, type, row, meta) {
                    //     return meta.row + meta.settings._iDisplayStart + 1;
                    // }
                },
                {
                    data: 'jabatan',
                    name: 'jabatan'
                },
                {
                    data: 'pendidikan',
                    name: 'pendidikan'
                },
                {
                    data: 'satker',
                    name: 'satker'
                },
                {
                    data: 'strata',
                    name: 'strata'
                },
                // {
                //     data: 'status',
                //     name: 'status'
                // }
            ]
        });
    });
</script>


    <!--app JS-->
    <script src="{{ URL::asset('assets/amdash/assets/js/app.js') }}"></script>

</body>

</html>