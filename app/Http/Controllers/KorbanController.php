<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Korban;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;

use App\Exports\KorbanExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\DB;

use PDF;

class KorbanController extends Controller
{
    public function index() {
        $title = "Dashboard";
        $subtitle = "BPBD DKI JAKARTA";

         // Get data tabel Korban
        $korban= Korban::join('kabupatens', 'kabupatens.id', '=', 'korbans.id_kab')
                        ->join('kecamatans', 'kecamatans.id', '=', 'korbans.id_kec')
                        ->join('kelurahans', 'kelurahans.id', '=', 'korbans.id_kel') 
                        ->orderBy('korbans.updated_at', 'DESC')
                        ->get(['korbans.id as id', 'korbans.no_identitas', 'korbans.tanggal_lahir', 'korbans.nama_korban', 'korbans.jenis_kelamin', 'korbans.alamat', 'kecamatans.kecamatan', 'kelurahans.nama_kelurahan', 'kabupatens.kabupaten']);

        // return $korban;
        return view('panel.content.korban.index', ['title' => $title, "subtitle" => $subtitle, 'korban' => $korban]);
    }

    public function create() {
        $title = "Dashboard";
        $subtitle = "BPBD DKI JAKARTA";
        
        // Get data tabel Korban
        $korban= Korban::join('kabupatens', 'kabupatens.id', '=', 'korbans.id_kab')
                 ->join('kecamatans', 'kecamatans.id', '=', 'korbans.id_kec')
                 ->join('kelurahans', 'kelurahans.id', '=', 'korbans.id_kel') 
                 ->get();

        $kabupaten = Kabupaten::all();
        $kecamatan = Kecamatan::all();
        $kelurahan = Kelurahan::all();
        
        return view('panel.content.korban.add', ['title' => $title, "subtitle" => $subtitle, 'korban' => $korban, 'kabupaten' => $kabupaten, 'kecamatan' => $kecamatan, 'kelurahan' => $kelurahan]);
    }

    public function edit($id)
    {
        $title = "Dashboard";
        $subtitle = "BPBD DKI JAKARTA";
        
        $korban = Korban::findOrFail($id);

        $kabupaten = Kabupaten::all();
        $kecamatan = Kecamatan::all();
        $kelurahan = Kelurahan::all();
        return view('panel.content.korban.edit', ['title' => $title, "subtitle" => $subtitle, 'korban' => $korban, 'kabupaten' => $kabupaten, 'kecamatan' => $kecamatan, 'kelurahan' => $kelurahan]);
    }


    public function update(Request $request, $id) {
        $this->validate($request, [
            'no_identitas'     => 'required',
            'nama_korban'     => 'required'
        ]);

        $updatedata = Korban::findOrFail($id);

        $data = $updatedata->update([
            'no_identitas' => $request->no_identitas,
            'nama_korban' => $request->nama_korban,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'umur' => $request->umur,
            'id_kel' => $request->kelurahan,
            'id_kec' => $request->kecamatan,
            'id_kab' => $request->kabupaten,
            'tanggal_lahir' => $request->tanggal_lahir
        ]);

        return redirect()->back()->with('success','Berhasil Update');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'no_identitas'     => 'required',
            'nama_korban'     => 'required'
        ]);

        $data = Korban::create([
            'no_identitas' => $request->no_identitas,
            'nama_korban' => $request->nama_korban,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'umur' => $request->umur,
            'id_kel' => $request->kelurahan,
            'id_kec' => $request->kecamatan,
            'id_kab' => $request->kabupaten,
            'tanggal_lahir' => $request->tanggal_lahir
        ]);

        if($data) {
            return redirect('/dashboard/korban')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            return redirect('/dashboard/korban')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    public function destroy($id) {
        $data = Korban::find($id);
        $data->delete();
        return redirect()->back()
            ->with('success', 'Data Sudah Dihapus');
    }

    public function delete($id)
    {
        $flight = Korban::find($id);
        $flight->delete();
        return redirect()->back()
            ->with('success', 'Data Sudah Dihapus');
    }

    public function export_excel()
	{
		$korban= Korban::join('kabupatens', 'kabupatens.id', '=', 'korbans.id_kab')
                        ->join('kecamatans', 'kecamatans.id', '=', 'korbans.id_kec')
                        ->join('kelurahans', 'kelurahans.id', '=', 'korbans.id_kel') 
                        ->get(['korbans.id as id', 'korbans.no_identitas', 'korbans.tanggal_lahir', 'korbans.nama_korban', 'korbans.jenis_kelamin', 'korbans.alamat', 'kecamatans.kecamatan', 'kelurahans.nama_kelurahan', 'kabupatens.kabupaten']);

        return Excel::download(new KorbanExport($korban), 'korban.xlsx');
	}

    public function cetak_pdf()
    {
    	// $korban= Korban::join('kabupatens', 'kabupatens.id', '=', 'korbans.id_kab')
        //                 ->join('kecamatans', 'kecamatans.id', '=', 'korbans.id_kec')
        //                 ->join('kelurahans', 'kelurahans.id', '=', 'korbans.id_kel') 
        //                 ->get(['korbans.id as id', 'korbans.no_identitas', 'korbans.tanggal_lahir', 'korbans.nama_korban', 'korbans.jenis_kelamin', 'korbans.alamat', 'kecamatans.kecamatan', 'kelurahans.nama_kelurahan', 'kabupatens.kabupaten']);

        $korban = DB::select("SELECT kabupatens.kabupaten, COUNT(IF (korbans.umur BETWEEN 0 AND 3,1,Null)) AS balita, COUNT(IF (korbans.umur BETWEEN 4 AND 17,1,null)) as anak, COUNT(IF(korbans.umur BETWEEN 18 AND 21,1,null)) AS remaja, COUNT(IF(korbans.umur BETWEEN 22 AND 45,1,null)) AS dewasa, COUNT(IF(korbans.umur>45,1,null)) AS ortu from korbans JOIN kabupatens ON korbans.id_kab = kabupatens.id GROUP BY kabupatens.kabupaten;");

        // return $korban;
    	$pdf = PDF::loadview('panel.content.korban.laporan_pdf',['korban'=>$korban]);
    	return $pdf->download('laporan-korban-pdf');
    }

}
