<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // Index
    public function index() {
        $title = "Dashboard";
        $subtitle = "BPBD DKI JAKARTA";
        return view('panel.content.main.main', ['title' => $title, "subtitle" => $subtitle]);
    }
}
