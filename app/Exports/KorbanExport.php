<?php

namespace App\Exports;

use App\Models\Korban;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Illuminate\Support\Facades\DB;

class KorbanExport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return [
            'kd_dik' => $row['kd_dik'],
            'nama_dk' => $row['nama_dk'],
            'tahun' => $row['tahun'],
            'tgl_mulai' => $row['tgl_mulai'],
            'tgl_selesai' => $row['tgl_selesai'],
            'dasar' => $row['dasar'],
            'jenis_pendidikan' => $row['jenis_pendidikan']
        ];
    }
}
